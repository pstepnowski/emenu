# -*- coding: utf-8 -*-
__author__ = 'pstepnowski'

from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Count
from django.contrib import messages

from eMenu.utils.pagination import page_or_404

from .models import Menu
from .forms import MenuSortForm, ReportBugForm


def menu_list(request):
    qs = Menu.objects.annotate(meals_count=Count('meals')).filter(meals_count__gt=0)

    form = MenuSortForm(request.GET or None)
    if form.is_valid():
        sort_option = form.cleaned_data.get('sort_option')
        if sort_option:
            qs = qs.order_by(sort_option)

    page = page_or_404(request, qs, 6)
    return render(request, 'menu/menu_list.html', {'page': page, 'form': form})


def view_menu(request, menu_id, menu_slug):
    menu = get_object_or_404(Menu, pk=menu_id, slug=menu_slug)

    return render(request, 'menu/view_menu.html', {'menu': menu})


def report_bug(request):
    form = ReportBugForm(request.POST or None)
    if form.is_valid():
        form.save()
        messages.add_message(request, messages.INFO, 'Formularz został wysłany.')
        return redirect('menu:menu_list')

    return render(request, 'menu/report_bug.html', {'form': form})




