# -*- coding: utf-8 -*-
__author__ = 'pstepnowski'

from django.db import models
from django.utils.functional import cached_property

from eMenu.utils.fields import ImageField
from eMenu.utils.text import slugify

MENU_CATEGORIES = (
    ('snacks', 'Przekąski'),
    ('soups', 'Zupy'),
    ('main_dish', 'Główne danie'),
    ('desserts', 'Desery'),
    ('cold_drinks', 'Napoje'),
)


class Meal(models.Model):
    name = models.CharField('Nazwa', max_length=255, unique=True)
    description = models.TextField('Opis')
    image = ImageField(
        verbose_name='Zdjęcie', min_size=(230, 160), max_size=(1024, 1024),
        max_filesize=512*1024, blank=True, default='', upload_to='menu/meals')
    category = models.CharField('Kategoria', max_length=63, choices=MENU_CATEGORIES)

    class Meta:
        verbose_name = 'Danie'
        verbose_name_plural = 'Dania'

    def __unicode__(self):
        return self.name


class Menu(models.Model):
    name = models.CharField('Nazwa', max_length=255,)
    meals = models.ManyToManyField(Meal, verbose_name='meals', through='MenusMeals')
    slug = models.SlugField('slug', max_length=255, blank=True, editable=False)

    class Meta:
        verbose_name = 'Menu'
        verbose_name_plural = 'Menu'
        ordering = ['-id']

    def __unicode__(self):
        return self.name

    def clean(self):
        if not self.slug:
            self.slug = slugify(self.name)
        return super(Menu, self).clean()

    @models.permalink
    def get_absolute_url(self):
        return ('menu:view_menu', (), {
            'menu_id': self.pk, 'menu_slug': self.slug})

    @cached_property
    def meals_count(self):
        return self.meals.count()


class MenusMeals(models.Model):
    menu = models.ForeignKey(Menu, verbose_name='menu')
    meal = models.ForeignKey(Meal, verbose_name='meal')

    class Meta:
        verbose_name = 'Danie'
        verbose_name_plural = 'Dania'
        unique_together = ('menu', 'meal',)

    def __unicode__(self):
        return u'%(meal)s w %(menu)s' % {
            'meal': unicode(self.meal), 'menu': unicode(self.menu)}


class UserMessage(models.Model):
    email = models.EmailField('E-mail', blank=True)
    description = models.TextField('Opis')
    created = models.DateTimeField('Zgłoszono', auto_now_add=True)

    class Meta:
        verbose_name = 'Zgłoszony bład'
        verbose_name_plural = 'Zgłoszone błędy'

    def __unicode__(self):
        return u'Zgłoszenie #%d' % (self.pk,)


