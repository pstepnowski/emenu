__author__ = 'pstepnowski'

from django.contrib import admin

from .models import Menu, Meal, MenusMeals, UserMessage


class MenusMealsInline(admin.TabularInline):
    model = MenusMeals
    extra = 0


class MealAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'category',)

admin.site.register(Meal, MealAdmin)


class MenuAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug',)
    inlines = (MenusMealsInline,)

admin.site.register(Menu, MenuAdmin)


class ReportBugAdmin(admin.ModelAdmin):
    list_display = ('id', 'description', 'email', 'created',)

admin.site.register(UserMessage, ReportBugAdmin)
