__author__ = 'pstepnowski'

from django.conf.urls import patterns, url

urlpatterns = patterns('eMenu.menu.views',
    url(r'^$', 'menu_list', name='menu_list'),
    url(r'^(?P<menu_id>\d{1,12})/(?P<menu_slug>[\w-]+)/$', 'view_menu', name='view_menu'),
    url(r'reportbug/$', 'report_bug', name='report_bug'),
)



