# -*- coding: utf-8 -*-
__author__ = 'pstepnowski'

from django import forms

from captcha.fields import CaptchaField

from .models import UserMessage

MENU_SORT_OPTIONS = (
    ('', '---'),
    ('-name', 'Nazwa - malejąco'),
    ('name', 'Nazwa - rosnąco'),
    ('-meals_count', 'Liczba dań - malejąco'),
    ('meals_count', 'Liczba dań - rosnąco'),
)


class MenuSortForm(forms.Form):
    sort_option = forms.ChoiceField(choices=MENU_SORT_OPTIONS, required=False, label='Sortuj wg')


class ReportBugForm(forms.ModelForm):
    captcha = CaptchaField()

    class Meta:
        model = UserMessage
        fields = ('email', 'description',)
