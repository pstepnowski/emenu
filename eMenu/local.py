
TEMPLATE_DEBUG = DEBUG = True

MANAGERS = ADMINS = (
    # ('Your Name', 'your_email@example.com'),
    ('pawel', 'stepnowski.pawel@gmail.com'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'eMenu',                           # Or path to database file if using sqlite3.
        'USER': 'root',                           # Not used with sqlite3.
        'PASSWORD': '',                       # Not used with sqlite3.
        'HOST': '',                           # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                           # Set to empty string for default. Not used with sqlite3.
    }
}

#CACHES = {
#    'default': {
#        #'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
#
#        #'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
#
#        #'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#        #'LOCATION': '127.0.0.1:11211',
#    }
#}

INTERNAL_IPS = ('127.0.0.1',)

EMAIL_HOST = 'localhost'
EMAIL_HOST_PASSWORD = ''
EMAIL_HOST_USER = ''
EMAIL_PORT = 25
EMAIL_USE_TLS = False