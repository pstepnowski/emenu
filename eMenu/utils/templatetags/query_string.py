from django.template import Library
from django.http import QueryDict


register = Library()


@register.simple_tag(takes_context=True)
def query_string(context, *remove, **add):
    '''
    Adds or removes query string parameters. Takes template context or raw
    query string as first parameter.
    '''
    if isinstance(context, basestring):
        # Context is not a context, but a raw query string.
        qs = QueryDict(context, mutable=True)
    else:
        qs = context['request'].GET.copy()

    # Remove parameters.
    for name in remove:
        if name in qs:
            del qs[name]

    # Add parameters or values.
    for name, value in add.iteritems():
        if name in qs and value is None:
            del qs[name]
        elif value is not None:
            if unicode(value) not in qs.getlist(name):
                qs.update({name: value})

    return qs.urlencode()