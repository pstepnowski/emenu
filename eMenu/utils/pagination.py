__author__ = 'pstepnowski'

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import Http404


def page_or_404(request, object_list, per_page, page_number=None, default_page=1):
    try:
        if page_number is None:
            page_number = request.GET.get('page', default_page)
        return Paginator(object_list, per_page).page(page_number)
    except (EmptyPage, PageNotAnInteger):
        raise Http404