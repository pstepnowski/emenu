try:
    from PIL import Image
except ImportError:
    import Image

from django.db.models import ImageField as DjangoImageField
from django.utils.translation import ugettext_lazy as _
from django.forms import ValidationError
from django.template.defaultfilters import filesizeformat

from south.modelsinspector import add_introspection_rules


add_introspection_rules([], ['^betfrog\.utils\.fields\.'])


class ImageField(DjangoImageField):
    default_error_messages = {
        'invalid_image': _(u'Upload a valid image. The file you uploaded was either not an image or a corrupted image.'),
        'min_size': _(u'Uploaded image is too small. Minimum dimensions are %(width)dx%(height)d px.'),
        'max_size': _(u'Uploaded image is too big. Maximum dimensions are %(width)dx%(height)d px.'),
        'max_filesize': _(u'Uploaded image size is too big. Maximum size is %(size)s.'),
        'allowed_formats': _(u'Uploaded image is not in allowed format. Allowed formats are: %(formats)s.'),
    }

    def __init__(self, verbose_name=None, min_size=None, max_size=None,
                 max_filesize=None, allowed_formats=('JPEG', 'PNG'),
                 *args, **kwargs):
        if 'max_length' not in kwargs:
            kwargs['max_length'] = 255
        super(ImageField, self).__init__(verbose_name, *args, **kwargs)
        self.min_size = min_size
        self.max_size = max_size
        self.max_filesize = max_filesize
        self.allowed_formats = allowed_formats

    def clean(self, *args, **kwargs):
        data = super(ImageField, self).clean(*args, **kwargs)

        try:
            trial_image = Image.open(data.file)
        except IOError:
            return data
        w, h = trial_image.size

        if self.max_filesize and data.file.size > self.max_filesize:
            raise ValidationError(self.error_messages['max_filesize'] % {
                'size': filesizeformat(self.max_filesize)})

        if self.min_size and (w < self.min_size[0] or h < self.min_size[1]):
            raise ValidationError(self.error_messages['min_size'] % {
                'width': self.min_size[0], 'height': self.min_size[1]})

        if self.max_size and (w > self.max_size[0] or h > self.max_size[1]):
            raise ValidationError(self.error_messages['max_size'] % {
                'width': self.max_size[0], 'height': self.max_size[1]})

        if self.allowed_formats and trial_image.format not in self.allowed_formats:
            raise ValidationError(self.error_messages['allowed_formats'] % {
                'formats': u', '.join(self.allowed_formats)})

        return data
