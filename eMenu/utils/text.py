__author__ = 'pstepnowski'

from unidecode import unidecode

from django.template.defaultfilters import slugify as django_slugify


def slugify(string):
    return django_slugify(unidecode(string))
