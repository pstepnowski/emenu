# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.31)
# Database: emenu
# Generation Time: 2014-02-22 17:30:25 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table auth_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_group`;

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_group_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_group_permissions`;

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_bda51c3c` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `group_id_refs_id_3cea63fe` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_a7792de1` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_permission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_permission`;

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_e4470c6e` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_728de91f` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`)
VALUES
	(1,'Can add permission',1,'add_permission'),
	(2,'Can change permission',1,'change_permission'),
	(3,'Can delete permission',1,'delete_permission'),
	(4,'Can add group',2,'add_group'),
	(5,'Can change group',2,'change_group'),
	(6,'Can delete group',2,'delete_group'),
	(7,'Can add user',3,'add_user'),
	(8,'Can change user',3,'change_user'),
	(9,'Can delete user',3,'delete_user'),
	(10,'Can add content type',4,'add_contenttype'),
	(11,'Can change content type',4,'change_contenttype'),
	(12,'Can delete content type',4,'delete_contenttype'),
	(13,'Can add session',5,'add_session'),
	(14,'Can change session',5,'change_session'),
	(15,'Can delete session',5,'delete_session'),
	(16,'Can add site',6,'add_site'),
	(17,'Can change site',6,'change_site'),
	(18,'Can delete site',6,'delete_site'),
	(19,'Can add log entry',7,'add_logentry'),
	(20,'Can change log entry',7,'change_logentry'),
	(21,'Can delete log entry',7,'delete_logentry'),
	(22,'Can add meal',8,'add_meal'),
	(23,'Can change meal',8,'change_meal'),
	(24,'Can delete meal',8,'delete_meal'),
	(25,'Can add menu',9,'add_menu'),
	(26,'Can change menu',9,'change_menu'),
	(27,'Can delete menu',9,'delete_menu'),
	(31,'Can add Danie',11,'add_menusmeals'),
	(32,'Can change Danie',11,'change_menusmeals'),
	(33,'Can delete Danie',11,'delete_menusmeals'),
	(34,'Can add kv store',12,'add_kvstore'),
	(35,'Can change kv store',12,'change_kvstore'),
	(36,'Can delete kv store',12,'delete_kvstore'),
	(37,'Can add Zgłoszony bład',13,'add_usermessage'),
	(38,'Can change Zgłoszony bład',13,'change_usermessage'),
	(39,'Can delete Zgłoszony bład',13,'delete_usermessage'),
	(40,'Can add captcha store',14,'add_captchastore'),
	(41,'Can change captcha store',14,'change_captchastore'),
	(42,'Can delete captcha store',14,'delete_captchastore');

/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_user`;

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;

INSERT INTO `auth_user` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `is_staff`, `is_active`, `is_superuser`, `last_login`, `date_joined`)
VALUES
	(1,'admin','','','stepnowski.pawel@gmail.com','pbkdf2_sha256$10000$bD8WmJSVAewp$LvQrtVGgf91wvh9emEXtxmHCtLLDyVeU/MpJCzJd5Gg=',1,1,1,'2014-02-22 17:30:08','2014-02-20 21:10:39');

/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_user_groups`;

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_fbfc09f1` (`user_id`),
  KEY `auth_user_groups_bda51c3c` (`group_id`),
  CONSTRAINT `user_id_refs_id_831107f1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `group_id_refs_id_f0ee9890` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_user_user_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_user_user_permissions`;

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_fbfc09f1` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `user_id_refs_id_f2045483` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `permission_id_refs_id_67e79cb` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table captcha_captchastore
# ------------------------------------------------------------

DROP TABLE IF EXISTS `captcha_captchastore`;

CREATE TABLE `captcha_captchastore` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `challenge` varchar(32) NOT NULL,
  `response` varchar(32) NOT NULL,
  `hashkey` varchar(40) NOT NULL,
  `expiration` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hashkey` (`hashkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `captcha_captchastore` WRITE;
/*!40000 ALTER TABLE `captcha_captchastore` DISABLE KEYS */;

INSERT INTO `captcha_captchastore` (`id`, `challenge`, `response`, `hashkey`, `expiration`)
VALUES
	(20,'WCIG','wcig','1a467771c4870c04e294311771cf77f387e9267a','2014-02-22 12:18:50'),
	(22,'FNLY','fnly','99681919a1b43a691d4902f2b98833d6d70b2161','2014-02-22 12:26:18'),
	(23,'NFJA','nfja','446ac81cdb2ca6b7195ebb3d40ef278f5d362f5c','2014-02-22 12:26:20'),
	(24,'GMWJ','gmwj','3793ec291be3155650101025678e7a0d1445c0ad','2014-02-22 12:26:26'),
	(25,'LDPK','ldpk','42615695f95283de7046e760c13ab761f2e996bc','2014-02-22 12:26:27'),
	(26,'AFPT','afpt','ddef629c62a9deb0f64c481cbe83861a44a13bdd','2014-02-22 12:26:29'),
	(27,'JGVD','jgvd','a5bb97865f2f359e2b468488218ee8d72ce04b26','2014-02-22 12:26:33'),
	(28,'IATL','iatl','238c800b6f44035fc0513ef1eb7c86847f5d5138','2014-02-22 12:26:34'),
	(29,'NAWL','nawl','b8922e3a27796eb2439fe615786648a8f2e6d1ee','2014-02-22 12:26:36'),
	(30,'OKWF','okwf','62d9b293df2004c8c864a5f502ccd781b4ba4d6f','2014-02-22 12:26:37'),
	(31,'CHAJ','chaj','b7b22b6977d8c67f15a4f79e2accf5b2f237d0d1','2014-02-22 12:26:38'),
	(32,'GGZD','ggzd','9a8a9fba018cbbb051a7a021b05b7cdab7c466e3','2014-02-22 12:26:39'),
	(33,'PBDJ','pbdj','4e8e4289c63abcb18e1bc5af643869d0e6b8ce4e','2014-02-22 12:35:45'),
	(34,'TLPI','tlpi','95101e8d91147e54b3b8aaf1c7601e529efc81ce','2014-02-22 12:35:57'),
	(35,'IPFC','ipfc','2700a49a31c7ae6fec2afe119ca83a3c321b2a74','2014-02-22 12:35:59'),
	(36,'UQUT','uqut','73cc3f375bc6ccc86d28ebf13d78021d9d599359','2014-02-22 12:36:07'),
	(37,'HHAK','hhak','db64e00d091887354870af2159a83e3dfc35f697','2014-02-22 12:36:30'),
	(38,'XKGM','xkgm','25138b6fa2af612b7b5bd55ccaa4299c1911572a','2014-02-22 12:39:36'),
	(39,'UQIZ','uqiz','687d4a108a5429aa5e964e253ef0ed69d6ff2884','2014-02-22 12:39:38'),
	(40,'IKRI','ikri','34321cf9e398486371bacf4312557a37224635b0','2014-02-22 12:39:40'),
	(41,'PBHA','pbha','537b5034274e5ce8688152a2c669f1e0d1d776c8','2014-02-22 12:41:48'),
	(42,'DOTO','doto','2b5b28e0572f1e43b4dc3ef4253acb0384dc0799','2014-02-22 12:43:45'),
	(43,'MVYG','mvyg','edd7f8eddc635959aa5d2ce311fb56150ec0293f','2014-02-22 12:43:47'),
	(44,'ZJAC','zjac','132ef72ef3a425dec2249b00e4715286a522bf84','2014-02-22 12:43:48'),
	(45,'AUQI','auqi','8471701c514014c07394ce4f09dd689079e49745','2014-02-22 12:43:53'),
	(46,'XXMI','xxmi','9042891363f2110622c9b2c78129a8810d693289','2014-02-22 12:44:33'),
	(47,'BRTL','brtl','84708ad26d2b12ac197fa4b826470f7aff608354','2014-02-22 12:44:53'),
	(48,'LEDB','ledb','462e0c897d728c61ee7e3365a382c43d620e09bf','2014-02-22 12:44:57'),
	(49,'NKAL','nkal','c5bd6ccdb5f6a544fdf696bcea472001e334d2cd','2014-02-22 12:46:03'),
	(50,'QWPS','qwps','299d0ae4770ab8d2e3e4de9273a3738fc17c9d51','2014-02-22 12:46:26'),
	(51,'DVOF','dvof','4880443160f510a21b9d1ff93dbd3a6302ce5cc7','2014-02-22 12:46:29'),
	(52,'IQJL','iqjl','f01aea49fd1395e64901cd9b9601890a367e3031','2014-02-22 12:46:43'),
	(53,'FGQO','fgqo','be0671e4a14039b8f2084277a65fa4130d76d345','2014-02-22 12:48:22'),
	(54,'IEBL','iebl','a1a22b15db2ae5022d03e3b981c120e3b3d579d1','2014-02-22 17:15:45');

/*!40000 ALTER TABLE `captcha_captchastore` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_admin_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_admin_log`;

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_fbfc09f1` (`user_id`),
  KEY `django_admin_log_e4470c6e` (`content_type_id`),
  CONSTRAINT `user_id_refs_id_c8665aa` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `content_type_id_refs_id_288599e6` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;

INSERT INTO `django_admin_log` (`id`, `action_time`, `user_id`, `content_type_id`, `object_id`, `object_repr`, `action_flag`, `change_message`)
VALUES
	(1,'2014-02-20 21:18:25',1,8,'1','Meal object',1,''),
	(2,'2014-02-20 21:19:16',1,9,'1','Menu object',1,''),
	(3,'2014-02-20 22:06:11',1,9,'1','Menu object',2,'Added menus \"Menus object\".'),
	(4,'2014-02-21 19:35:15',1,9,'1','Menu1',2,'Dodano Danie \"MenusMeals object\".'),
	(5,'2014-02-21 19:47:27',1,8,'1','Ryba po grecku',2,'Zmieniono name, description i image'),
	(6,'2014-02-21 19:48:48',1,8,'2','Karkówka w sosie',1,''),
	(7,'2014-02-21 19:49:20',1,8,'3','Karkówka ver2',1,''),
	(8,'2014-02-21 19:50:06',1,8,'4','Przekąska 1',1,''),
	(9,'2014-02-21 19:50:23',1,8,'5','Przekąska 2',1,''),
	(10,'2014-02-21 19:50:47',1,8,'6','Przekąska 3',1,''),
	(11,'2014-02-21 19:51:17',1,8,'7','Napój 1',1,''),
	(12,'2014-02-21 19:51:38',1,8,'8','Napój 2',1,''),
	(13,'2014-02-21 19:52:02',1,8,'9','Napój 3',1,''),
	(14,'2014-02-21 19:52:39',1,8,'10','Deser 1',1,''),
	(15,'2014-02-21 19:54:06',1,8,'11','Deser 2',1,''),
	(16,'2014-02-21 19:54:39',1,8,'12','Deser 3',1,''),
	(17,'2014-02-21 19:56:02',1,8,'13','Zupa 1',1,''),
	(18,'2014-02-21 19:56:21',1,8,'14','Zupa 2',1,''),
	(19,'2014-02-21 19:56:40',1,8,'15','Zupa 3',1,''),
	(20,'2014-02-21 19:58:37',1,9,'1','Menu1',2,'Dodano Danie \"Karkówka w sosie w Menu1\". Dodano Danie \"Karkówka ver2 w Menu1\". Dodano Danie \"Przekąska 1 w Menu1\". Dodano Danie \"Przekąska 3 w Menu1\". Dodano Danie \"Napój 1 w Menu1\". Dodano Danie \"Napój 2 w Menu1\". Dodano Danie \"Deser 1 w Menu1\". Dodano Danie \"Deser 2 w Menu1\". Dodano Danie \"Zupa 1 w Menu1\". Dodano Danie \"Zupa 2 w Menu1\".'),
	(21,'2014-02-21 20:01:47',1,9,'1','Zestaw dań numer 1',2,'Zmieniono name'),
	(22,'2014-02-21 20:02:51',1,9,'2','Zestaw dań numer 2',1,''),
	(23,'2014-02-21 20:03:36',1,9,'3','Zestaw dań numer 3',1,''),
	(24,'2014-02-21 21:50:25',1,9,'4','',3,''),
	(25,'2014-02-21 21:53:57',1,9,'3','Zestaw dań numer 3',2,'Żadne pole nie zmienione.'),
	(26,'2014-02-21 21:54:54',1,9,'2','Zestaw dań numer 2',2,'Żadne pole nie zmienione.'),
	(27,'2014-02-21 21:55:01',1,9,'1','Zestaw dań numer 1',2,'Żadne pole nie zmienione.'),
	(28,'2014-02-22 09:04:34',1,9,'1','Zestaw dań numer 1',2,'Usunięto Danie \"Przekąska 3 w Zestaw dań numer 1\".'),
	(29,'2014-02-22 09:05:04',1,9,'2','Zestaw dań numer 2',2,'Usunięto Danie \"Deser 3 w Zestaw dań numer 2\". Usunięto Danie \"Przekąska 3 w Zestaw dań numer 2\".'),
	(30,'2014-02-22 09:05:21',1,9,'3','Zestaw dań numer 3',2,'Usunięto Danie \"Przekąska 3 w Zestaw dań numer 3\".'),
	(31,'2014-02-22 17:07:26',1,9,'4','Zestaw dań numer 4',1,''),
	(32,'2014-02-22 17:07:48',1,9,'5','Zestaw dań numer 5',1,''),
	(33,'2014-02-22 17:08:26',1,9,'6','Zestaw dań numer 6',1,''),
	(34,'2014-02-22 17:09:00',1,9,'7','Zestaw dań numer 7',1,''),
	(35,'2014-02-22 17:09:23',1,9,'8','Zestaw dań numer 7',1,''),
	(36,'2014-02-22 17:09:33',1,9,'8','Zestaw dań numer 8',2,'Zmieniono name');

/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_content_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_content_type`;

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`)
VALUES
	(1,'permission','auth','permission'),
	(2,'group','auth','group'),
	(3,'user','auth','user'),
	(4,'content type','contenttypes','contenttype'),
	(5,'session','sessions','session'),
	(6,'site','sites','site'),
	(7,'log entry','admin','logentry'),
	(8,'meal','menu','meal'),
	(9,'menu','menu','menu'),
	(11,'Danie','menu','menusmeals'),
	(12,'kv store','thumbnail','kvstore'),
	(13,'Zgłoszony bład','menu','usermessage'),
	(14,'captcha store','captcha','captchastore');

/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_session
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_session`;

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_c25c2c28` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`)
VALUES
	('4c9789a0fbe2bfa668dcaed19d59a77c','YmQwOTA5MzQ0YjA3Yjk2NGJjMzk4N2RlM2JhMDIzY2FhM2Y1OWQ5ZTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2014-03-06 21:45:23'),
	('5b55cd1fcd190ad29739149ccf162140','YmQwOTA5MzQ0YjA3Yjk2NGJjMzk4N2RlM2JhMDIzY2FhM2Y1OWQ5ZTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2014-03-08 17:30:08'),
	('e8e473e543f877f4d552532a86cfbed3','YmQwOTA5MzQ0YjA3Yjk2NGJjMzk4N2RlM2JhMDIzY2FhM2Y1OWQ5ZTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2014-03-06 21:13:12');

/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_site
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_site`;

CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;

INSERT INTO `django_site` (`id`, `domain`, `name`)
VALUES
	(1,'example.com','example.com');

/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menu_meal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu_meal`;

CREATE TABLE `menu_meal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(255) NOT NULL,
  `category` varchar(63) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `menu_meal` WRITE;
/*!40000 ALTER TABLE `menu_meal` DISABLE KEYS */;

INSERT INTO `menu_meal` (`id`, `name`, `description`, `image`, `category`)
VALUES
	(1,'Ryba po grecku','Ryba po grecku......','menu/meals/ryb2.jpeg','main_dish'),
	(2,'Karkówka w sosie','Karkówka w sosie.....','menu/meals/karkowka2.jpg','main_dish'),
	(3,'Karkówka ver2','Karkówka ver2 .....','menu/meals/karkowka.jpeg','main_dish'),
	(4,'Przekąska 1','Przekąska 1....','menu/meals/przekąska4.jpg','snacks'),
	(5,'Przekąska 2','Przekąska 2......','menu/meals/przekaska2.jpg','snacks'),
	(6,'Przekąska 3','Przekąska 3 ....','','snacks'),
	(7,'Napój 1','Napój 1....','menu/meals/napoj1.jpg','cold_drinks'),
	(8,'Napój 2','Napój 2....','menu/meals/napoj2.jpg','cold_drinks'),
	(9,'Napój 3','Napój 3...','menu/meals/napoj3.jpg','cold_drinks'),
	(10,'Deser 1','Deser 1...','menu/meals/deser.jpeg','desserts'),
	(11,'Deser 2','Deser 2...','menu/meals/deser222.jpg','desserts'),
	(12,'Deser 3','Deser 3...','','desserts'),
	(13,'Zupa 1','Zupa 1...','menu/meals/zupa1.jpg','soups'),
	(14,'Zupa 2','Zupa 2...','menu/meals/zupa2.jpg','soups'),
	(15,'Zupa 3','Zupa 3...','menu/meals/zupa3.jpg','soups');

/*!40000 ALTER TABLE `menu_meal` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menu_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu_menu`;

CREATE TABLE `menu_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `menu_menu` WRITE;
/*!40000 ALTER TABLE `menu_menu` DISABLE KEYS */;

INSERT INTO `menu_menu` (`id`, `name`, `slug`)
VALUES
	(1,'Zestaw dań numer 1','zestaw-dan-numer-1'),
	(2,'Zestaw dań numer 2','zestaw-dan-numer-2'),
	(3,'Zestaw dań numer 3','zestaw-dan-numer-3'),
	(4,'Zestaw dań numer 4','zestaw-dan-numer-4'),
	(5,'Zestaw dań numer 5','zestaw-dan-numer-5'),
	(6,'Zestaw dań numer 6','zestaw-dan-numer-6'),
	(7,'Zestaw dań numer 7','zestaw-dan-numer-7'),
	(8,'Zestaw dań numer 8','zestaw-dan-numer-7');

/*!40000 ALTER TABLE `menu_menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menu_menusmeals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu_menusmeals`;

CREATE TABLE `menu_menusmeals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `meal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu_id` (`menu_id`,`meal_id`),
  KEY `menu_menusmeals_143efa3` (`menu_id`),
  KEY `menu_menusmeals_cbe177c7` (`meal_id`),
  CONSTRAINT `meal_id_refs_id_2c1aa4f8` FOREIGN KEY (`meal_id`) REFERENCES `menu_meal` (`id`),
  CONSTRAINT `menu_id_refs_id_9bd76034` FOREIGN KEY (`menu_id`) REFERENCES `menu_menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `menu_menusmeals` WRITE;
/*!40000 ALTER TABLE `menu_menusmeals` DISABLE KEYS */;

INSERT INTO `menu_menusmeals` (`id`, `menu_id`, `meal_id`)
VALUES
	(2,1,1),
	(3,1,2),
	(4,1,3),
	(5,1,4),
	(7,1,7),
	(8,1,8),
	(9,1,10),
	(10,1,11),
	(11,1,13),
	(12,1,14),
	(13,2,2),
	(21,2,4),
	(17,2,7),
	(14,2,8),
	(23,2,9),
	(18,2,10),
	(16,2,11),
	(15,2,13),
	(20,2,15),
	(24,3,1),
	(26,3,4),
	(30,3,5),
	(25,3,8),
	(27,3,13),
	(29,3,14),
	(31,4,1),
	(32,4,3),
	(33,4,7),
	(34,4,12),
	(35,4,14),
	(36,5,2),
	(40,5,3),
	(38,5,5),
	(39,5,8),
	(37,5,11),
	(44,6,3),
	(41,6,4),
	(46,6,7),
	(45,6,10),
	(47,6,11),
	(42,6,12),
	(43,6,13),
	(50,7,1),
	(48,7,4),
	(49,7,10),
	(51,8,3),
	(54,8,7),
	(53,8,12),
	(52,8,13);

/*!40000 ALTER TABLE `menu_menusmeals` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menu_usermessage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu_usermessage`;

CREATE TABLE `menu_usermessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(75) NOT NULL,
  `description` longtext NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table thumbnail_kvstore
# ------------------------------------------------------------

DROP TABLE IF EXISTS `thumbnail_kvstore`;

CREATE TABLE `thumbnail_kvstore` (
  `key` varchar(200) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `thumbnail_kvstore` WRITE;
/*!40000 ALTER TABLE `thumbnail_kvstore` DISABLE KEYS */;

INSERT INTO `thumbnail_kvstore` (`key`, `value`)
VALUES
	('sorl-thumbnail||image||015039ed029d9fe7e886fd80c6d13c98','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/napoj3.jpg\", \"size\": [600, 600]}'),
	('sorl-thumbnail||image||02d30408e323c5fe838cb9a29869a362','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/ef/50/ef50b8a9a4a671517f94f9699a3cf8f6.jpg\", \"size\": [100, 100]}'),
	('sorl-thumbnail||image||1470f273932f4c31b57a52ef05c0546b','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/e6/fb/e6fb0e98b5cae850aaea35a4db54d86f.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||image||1bdda0533dc14453f50a90f88619da62','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/karkowka.jpeg\", \"size\": [259, 194]}'),
	('sorl-thumbnail||image||28255ac95f5c067bc291fcd7929d1954','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/napoj2.jpg\", \"size\": [600, 600]}'),
	('sorl-thumbnail||image||2f46e90304123857f7c678bdaa3f8f10','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/b6/73/b6739a2ffe909ffad0b9376479b3076b.jpg\", \"size\": [200, 200]}'),
	('sorl-thumbnail||image||3595d914f524bb788d4413ae17a60797','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/karkowka2.jpg\", \"size\": [567, 319]}'),
	('sorl-thumbnail||image||3dd682559614ec2da7219d632abfd49c','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/d4/87/d4873946a6096e6ff9574b9df01759e3.jpg\", \"size\": [200, 200]}'),
	('sorl-thumbnail||image||3f1e19285674c7c0b06c95c5204cc977','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/zupa2.jpg\", \"size\": [532, 375]}'),
	('sorl-thumbnail||image||4910434adf618e4d8cb8fa7e4c77a926','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/39/fe/39fedf151ba1d20e5e5391b2e2af6191.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||image||4a71104455bf3780fa189c8149540f1d','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/7e/6d/7e6d8f058629531b7cb3a430e36794e6.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||image||4cbd93b5ea2da2c0151e413f44c99540','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/61/33/6133b0b6e104ad7e62e91c53d088dc6a.jpg\", \"size\": [100, 100]}'),
	('sorl-thumbnail||image||4e9ce97e0d5f5ec1e9f4335bcfaf18d5','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/33/e5/33e52fa38c6fe35fceec32917dc21bba.jpg\", \"size\": [100, 100]}'),
	('sorl-thumbnail||image||50c145724d045930b59f4820e4b0ccad','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/35/2f/352f94106f9a5584a5a3d134d0ea7bcb.jpg\", \"size\": [100, 100]}'),
	('sorl-thumbnail||image||54444cc4e6678970550d9b9a63b480a7','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/napoj1.jpg\", \"size\": [600, 600]}'),
	('sorl-thumbnail||image||549678b8f059afe5f2de7bcc95e2a885','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/1d/30/1d3062a1c116069338ec322a8a3ce3e6.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||image||60455a181fc218692f12496f39b2dcc2','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/zupa1.jpg\", \"size\": [900, 606]}'),
	('sorl-thumbnail||image||618528678e221f12dd4adcc5fbd3e947','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/3a/10/3a10a0819f9049e5ecceb898f4eec032.jpg\", \"size\": [100, 100]}'),
	('sorl-thumbnail||image||79af3bf05cc7511ed00d236d80f9c5e6','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/76/09/7609b31e616ebd30f71659531c17fd05.jpg\", \"size\": [100, 100]}'),
	('sorl-thumbnail||image||801532760ac5e5d260bb55b84cb865f3','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/deser222.jpg\", \"size\": [703, 600]}'),
	('sorl-thumbnail||image||830a4a4741f7180bd800dcb6690bc201','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/44/76/4476f9021f2f88a265758b2a087df14c.jpg\", \"size\": [200, 200]}'),
	('sorl-thumbnail||image||83518f2936417f0234d15c6f9e37414c','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/44/19/44191ebf7ae6d6530ca818315fa3d400.jpg\", \"size\": [200, 200]}'),
	('sorl-thumbnail||image||87ab3529c8bd4d5dda4ce834c90a121a','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/01/98/01983c9f7e107fb52a6af86d0443bc5e.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||image||984c3b2f137a862525376cf958be31fd','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/72/f0/72f0f97aec05c3155a780277a9a6f067.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||image||9bef25b067a8000a706bdeae3698305e','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/87/ad/87ad3c6d140c7d3786ee0268e1062a0e.jpg\", \"size\": [200, 200]}'),
	('sorl-thumbnail||image||9d09715bc6a5a4f62a17b9dbd2c46252','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/5f/d0/5fd059b4ed31b4d9c72f4e23242f6975.jpg\", \"size\": [200, 200]}'),
	('sorl-thumbnail||image||9f317c267741368972a72cea654579cf','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/zupa3.jpg\", \"size\": [400, 300]}'),
	('sorl-thumbnail||image||a0034277aaf01db9a8397163b548853c','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/aa/ec/aaec3568428f120d9a0b540b61d0aad0.jpg\", \"size\": [100, 100]}'),
	('sorl-thumbnail||image||ae0faf1e94c5d8c5a35e802a33279756','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/89/c3/89c3abc034bd04a91b967908101a403b.jpg\", \"size\": [100, 100]}'),
	('sorl-thumbnail||image||b3f0cb5e27116e6071ae06d31be75fa1','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/5c/54/5c54f023cee16e23a9216a6333d45a0d.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||image||b7bb1ba4e02e03f1eba4cea8c95fcd4e','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/de/7e/de7e1307b44aa4efc82322e6f4330ae2.jpg\", \"size\": [200, 200]}'),
	('sorl-thumbnail||image||b9686ed8199a2bb0b9d22fdd7597967e','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/8f/84/8f847b22577e46864f88e9437ee0672f.jpg\", \"size\": [100, 100]}'),
	('sorl-thumbnail||image||be1c77bcf3fb229ae02df49fe02f474d','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/2b/dc/2bdc673d60da6f042a1e59817b96d057.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||image||cae796f193dd09b0139ffad615e64e16','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/07/3d/073d200aa629b266adf268e5ba026034.jpg\", \"size\": [200, 200]}'),
	('sorl-thumbnail||image||cbcb184f0100eedd16af95b9957339c4','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/e8/46/e8461445cf13048b4abad37f68456a74.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||image||ceb15f92c7012ecd85de6e6466d65d5e','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/98/4c/984c21da83c32705b81d376d4f65bee7.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||image||e8ec4e3e4a033ef4c06e25dfeb3cf914','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/d7/42/d7421010820bc32fa0d258f446324fd9.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||image||eb079857529fe2a9019e94cc2beae174','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/f5/17/f517cf04d5e43d874bb0d8d61f6589f6.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||image||f4b1a4403dff529ba57d38d28a1d5323','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/deser.jpeg\", \"size\": [254, 198]}'),
	('sorl-thumbnail||image||f77f08deef038a976db48c3af3cc9f7c','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/przek\\u0105ska4.jpg\", \"size\": [600, 452]}'),
	('sorl-thumbnail||image||f7dd178a3594c178297fb1ebd92a7689','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/ryb2.jpeg\", \"size\": [275, 183]}'),
	('sorl-thumbnail||image||f7e4718df6d58c25c35922b10855d8b4','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/8d/a9/8da972089697a0f7de6e90974025b263.jpg\", \"size\": [200, 200]}'),
	('sorl-thumbnail||image||fbccf3e001091037db4e8c8982ecafb0','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"menu/meals/przekaska2.jpg\", \"size\": [1024, 768]}'),
	('sorl-thumbnail||image||fd64f2f1f6864b254bd4217d857b2af7','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/22/27/2227ad802acfc69203d931be39975d4b.jpg\", \"size\": [140, 100]}'),
	('sorl-thumbnail||thumbnails||015039ed029d9fe7e886fd80c6d13c98','[\"f7e4718df6d58c25c35922b10855d8b4\", \"ae0faf1e94c5d8c5a35e802a33279756\", \"4a71104455bf3780fa189c8149540f1d\"]'),
	('sorl-thumbnail||thumbnails||1bdda0533dc14453f50a90f88619da62','[\"e8ec4e3e4a033ef4c06e25dfeb3cf914\"]'),
	('sorl-thumbnail||thumbnails||28255ac95f5c067bc291fcd7929d1954','[\"be1c77bcf3fb229ae02df49fe02f474d\", \"4cbd93b5ea2da2c0151e413f44c99540\", \"2f46e90304123857f7c678bdaa3f8f10\"]'),
	('sorl-thumbnail||thumbnails||3595d914f524bb788d4413ae17a60797','[\"79af3bf05cc7511ed00d236d80f9c5e6\", \"cae796f193dd09b0139ffad615e64e16\", \"1470f273932f4c31b57a52ef05c0546b\"]'),
	('sorl-thumbnail||thumbnails||3f1e19285674c7c0b06c95c5204cc977','[\"4910434adf618e4d8cb8fa7e4c77a926\"]'),
	('sorl-thumbnail||thumbnails||54444cc4e6678970550d9b9a63b480a7','[\"cbcb184f0100eedd16af95b9957339c4\", \"50c145724d045930b59f4820e4b0ccad\", \"830a4a4741f7180bd800dcb6690bc201\"]'),
	('sorl-thumbnail||thumbnails||60455a181fc218692f12496f39b2dcc2','[\"ceb15f92c7012ecd85de6e6466d65d5e\", \"b9686ed8199a2bb0b9d22fdd7597967e\", \"83518f2936417f0234d15c6f9e37414c\"]'),
	('sorl-thumbnail||thumbnails||801532760ac5e5d260bb55b84cb865f3','[\"fd64f2f1f6864b254bd4217d857b2af7\", \"618528678e221f12dd4adcc5fbd3e947\", \"b7bb1ba4e02e03f1eba4cea8c95fcd4e\"]'),
	('sorl-thumbnail||thumbnails||9f317c267741368972a72cea654579cf','[\"9bef25b067a8000a706bdeae3698305e\", \"984c3b2f137a862525376cf958be31fd\", \"a0034277aaf01db9a8397163b548853c\"]'),
	('sorl-thumbnail||thumbnails||f4b1a4403dff529ba57d38d28a1d5323','[\"9d09715bc6a5a4f62a17b9dbd2c46252\", \"eb079857529fe2a9019e94cc2beae174\", \"02d30408e323c5fe838cb9a29869a362\"]'),
	('sorl-thumbnail||thumbnails||f77f08deef038a976db48c3af3cc9f7c','[\"3dd682559614ec2da7219d632abfd49c\", \"87ab3529c8bd4d5dda4ce834c90a121a\", \"4e9ce97e0d5f5ec1e9f4335bcfaf18d5\"]'),
	('sorl-thumbnail||thumbnails||f7dd178a3594c178297fb1ebd92a7689','[\"549678b8f059afe5f2de7bcc95e2a885\"]'),
	('sorl-thumbnail||thumbnails||fbccf3e001091037db4e8c8982ecafb0','[\"b3f0cb5e27116e6071ae06d31be75fa1\"]');

/*!40000 ALTER TABLE `thumbnail_kvstore` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
